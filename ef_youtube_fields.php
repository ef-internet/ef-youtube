<?php

add_filter( 'acf/settings/load_json', 'ef_youtube_load_json' );
function ef_youtube_load_json( $paths ){

	$paths[] = plugin_dir_path( __FILE__ ) . '/acf-json';

	return $paths;
}
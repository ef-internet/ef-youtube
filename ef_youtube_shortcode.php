<?php

add_shortcode( 'ef_youtube', 'ef_youtube_output');
function ef_youtube_output($atts){

	$atts = shortcode_atts( 
		array(
			'playlist' => '',
			'limit' => 25,
			'id' => '',
			'width' => '640',
			'height' => '360',
			'layout' => 'columns',
			'show_items' => 3,
			'slide_items' => 3,
			'initial_item' => 0,
			'more_button_text' => 'Mehr anzeigen <i class="uk-icon-angle-down"></i>',
			'less_button_text' => 'Weniger anzeigen <i class="uk-icon-angle-up"></i>',
			'items_list_title' => 'Weitere Videos',
			'search' => 'yes',
			'search_placeholder' => 'Suchen...',
			'scroll_offset' => 150
		), $atts 
	);
	$atts['search'] = $atts['search'] == 'yes' ? true : false;

	$yt_data = '';
	$ef_yt = array();
	$api_key = 'AIzaSyDtocnMgStOSXx3AbdK5H4cVSwThf9ZBCw';
	$base_url = 'https://www.googleapis.com/youtube/v3/';
	$transient = 'ef_youtube_' . md5( serialize( $atts ) );
	$show_more = false;
	$origin = parse_url( site_url() );
/*	delete_transient( $transient );*/


	if ( false === $yt_data = get_transient( $transient ) ):

		if ( $atts['playlist'] ):

			$json = wp_remote_get( $base_url.'playlistItems?part=snippet&maxResults='.$atts['limit'].'&playlistId='.$atts['playlist'].'&fields=items(snippet(description%2CresourceId%2FvideoId%2Cthumbnails%2Fmedium%2Ctitle))&key='.$api_key );

		elseif ( $atts['id'] ):

			$json = wp_remote_get( $base_url.'videos?part=snippet&id='.$atts['id'].'&fields=items(id%2Csnippet(description%2Cthumbnails%2Fmedium%2Ctitle))&key='.$api_key );
		endif;

		if ( wp_remote_retrieve_response_code( $json ) === 200 ):

			$yt_data = json_decode( wp_remote_retrieve_body( $json ), true );

			// cache api response
			set_transient( $transient, $yt_data,  24 * HOUR_IN_SECONDS );

		else:
			if ( function_exists(console_log) ):
				console_log(wp_remote_retrieve_response_code( $json ));
			endif;
		endif;

	endif;

	if ( ! $yt_data || empty($yt_data['items'] ) ){
		return '';
	}

	$yt_data['initial_item'] = $atts['initial_item'];

	// parse and remap data
	foreach ( $yt_data['items'] as $key => $item ) {
		$_key = $key;
		$key = $atts['playlist'] ? $item['snippet']['resourceId']['videoId'] : $item['id'];
		$yt_data['items'][$key]['title'] = '<span class="ef-yt-title">' . $item['snippet']['title'] . '</span>';
		$yt_data['items'][$key]['description'] = '<div class="ef-yt-description">' . $item['snippet']['description'] . '</div>';
		if ( false !== $show_more = strpos( $yt_data['items'][$key]['description'], '-----' ) ){
			$toggle = '<button class="uk-button ef-yt-show-more" data-ef-state="0" data-uk-toggle="{target:\'.ef-yt-more\', animation:\'uk-animation-slide-bottom\'}">' . $atts['more_button_text'] . '</button>' . "\n";
			$toggle .= '<div class="ef-yt-more uk-hidden">' . "\n";
			$yt_data['items'][$key]['description'] = substr_replace( $yt_data['items'][$key]['description'], $toggle, $show_more, 0 ) . "\n" . '</div>';
		}
		$yt_data['items'][$key]['thumb'] = $item['snippet']['thumbnails']['medium'];
		// make sure initial item is not a number
		if ( is_numeric($atts['initial_item']) && $atts['initial_item'] == $_key ){
			$yt_data['initial_item'] = $key;
		}
		unset($yt_data['items'][$_key]);
	}

	ob_start(); ?>

	<div class="uk-grid ef-yt-container" data-uk-grid-margin>
		<div class="<?php echo $atts['layout'] == 'columns' ? 'uk-width-medium-3-4' : 'uk-width-1-1'; ?>">
			<div class="ef-responsive-video">
				<iframe id="ef-yt-player" src="https://www.youtube.com/embed/<?php echo esc_attr($yt_data['initial_item']); ?>?enablejsapi=1&rel=0&origin=<?php echo $origin['scheme'] . '://' . $origin['host']; ?>" frameborder="0" width="<?php echo esc_attr($atts['width']); ?>" height="<?php echo esc_attr($atts['height']); ?>" allowfullscreen allow="autoplay"></iframe>
			</div>
			<div class="ef-yt-video-details">
				<?php
				echo $yt_data['items'][$yt_data['initial_item']]['title'];
				echo $yt_data['items'][$yt_data['initial_item']]['description'];
				?>
			</div>
		</div>
		<div class="<?php echo $atts['layout'] == 'columns' ? 'uk-width-medium-1-4' : 'uk-width-1-1'; ?>">
			<h3><?php echo esc_html($atts['items_list_title']); ?></h3>
			<?php if ( $atts['search'] ){ ?>
				<input id="ef-yt-search-filter" type="search" placeholder="<?php echo $atts['search_placeholder']; ?>" class="uk-width-1-1 uk-margin-bottom">
			<?php } ?>
			<div class="ef-yt-item-list" data-slick='{"slidesToShow": <?php echo esc_attr($atts['show_items']); ?>, "slidesToScroll": <?php echo esc_attr($atts['slide_items']); ?>}'>
			<?php
				foreach ( $yt_data['items'] as $key => $item ) { ?>
					<figure class="uk-overlay" data-yt-id="<?php echo $key; ?>">
						<img src="<?php echo $item['thumb']['url']; ?>" width="<?php echo $item['thumb']['width']; ?>" height="<?php echo $item['thumb']['height']; ?>">
						<figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom">
							<?php echo $item['title']; ?>
						</figcaption>
					</figure>
				<?php 
				} ?>
			</div>
		</div>
	</div>
	<?php
	$yt_data['buttons'] = array(
		'button_more' => $atts['more_button_text'],
		'button_less' => $atts['less_button_text']
	);
	$yt_data['layout'] = $atts['layout'];
	$yt_data['search'] = $atts['search'];
	$yt_data['scroll_offset'] = $atts['scroll_offset'];

	// enqueue frontend javascript and css
	wp_enqueue_script( 'ef-slick' );
	wp_enqueue_style( 'ef-slick' );
	wp_enqueue_style( 'ef-slick-theme' );
	wp_enqueue_script( 'ef-youtube' );
	wp_add_inline_script( 'ef-youtube', 'var efYtData = ' . json_encode( $yt_data ) . ';', $position = 'before' );

	return ob_get_clean();
}
<?php

add_action( 'wp_enqueue_scripts', function(){
	wp_register_script( 'ef-slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array('jquery') );
	wp_register_script( 'ef-youtube', plugin_dir_url( __FILE__ ) . 'js/ef-youtube.js', array( 'jquery', 'ef-slick' ), filemtime( plugin_dir_path( __FILE__ ) . '/js/ef-youtube.js') );
});
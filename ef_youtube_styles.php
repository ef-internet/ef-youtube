<?php

add_action( 'beans_uikit_enqueue_scripts', 'ef_youtube_enqueue_less_fragment' );

function ef_youtube_enqueue_less_fragment(){
	beans_compiler_add_fragment( 'uikit', plugin_dir_path( __FILE__ ) . '/styles/ef_youtube.less', 'less' );
}

add_action( 'wp_enqueue_scripts', function(){
	wp_register_style( 'ef-slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css' );
	wp_register_style( 'ef-slick-theme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css' );
});
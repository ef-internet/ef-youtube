<?php
/*
Plugin Name: EF YouTube
Description: Erstellt den shortcode ef_youtube zur Ausgabe von YouTube-Playlists oder Videos in einem Player mit Thumbnail-Slider.
Author: Timo Klemm
Version: 0.1.8
Author URI: https://github.com/team-ok
*/

require_once( plugin_dir_path( __FILE__ ) . 'ef_youtube_fields.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_youtube_styles.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_youtube_scripts.php' );

require_once( plugin_dir_path( __FILE__ ) . 'ef_youtube_shortcode.php' );

add_action( 'beans_uikit_enqueue_scripts', 'ef_yt_enqueue_uikit', PHP_INT_MAX );
function ef_yt_enqueue_uikit(){

	global $_beans_uikit_enqueued_items;

	if ( ! in_array( 'toggle', $_beans_uikit_enqueued_items['components']['core'] ) ){
		beans_uikit_enqueue_components( array( 'toggle' ) );
	}
	if ( ! in_array( 'animation', $_beans_uikit_enqueued_items['components']['core'] ) ){
		beans_uikit_enqueue_components( array( 'animation' ) );
	}
}
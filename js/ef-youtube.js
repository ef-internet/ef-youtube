var efYtPlayer;
var efYtItemList;
var efYtIframeApiTag = document.createElement('script');
efYtIframeApiTag.src = "https://www.youtube.com/iframe_api";
var efYtFirstScriptTag = document.getElementsByTagName('script')[0];
var searchFilter;

efYtFirstScriptTag.parentNode.insertBefore(efYtIframeApiTag, efYtFirstScriptTag);

function onYouTubeIframeAPIReady() {
  efYtPlayer = new YT.Player('ef-yt-player');

  ( function($){

    efYtItemList.on('click', '.slick-slide', function(){
      var i = $(this).find('[data-yt-id]').data('yt-id');
      if ( $(window).width() < 768 ){
      	$('html,body').animate({scrollTop: $(efYtPlayer.getIframe()).offset().top - efYtData.scroll_offset}, 'slow');
      }
      efYtPlayer.loadVideoById({
        'videoId': i,
        'suggestedQuality': 'large'
      });
      efYtPlayer.playVideo();
      $(efYtPlayer.a).closest('.ef-yt-container').find('.ef-yt-video-details').html(
        efYtData.items[i]['title'] + "\n" + efYtData.items[i]['description']
      );

      efYtItemList.slick('slickUnfilter').slick('slickFilter', $(this).siblings() );
    });

    if ( efYtData.search ){
    	searchFilter.on('keyup', function(){
    	  var textinput = jQuery(this).val();
    	  var re = new RegExp(textinput, 'i');
    	  
    	  efYtItemList.slick('slickUnfilter').slick('slickFilter', function(index){
    	    return $(this).text().search(re) > -1 ? true : false;      
    	  });
    	});
    }



  }(jQuery) );
}

( function($){

  $(document).ready(function(){

  	if ( efYtData.search ){
    	searchFilter = $('#ef-yt-search-filter');
    }

    $('.ef-yt-video-details').on( 'click', '.ef-yt-show-more', function(){
      if ( $(this).data('ef-state') === 0 ){
        $(this).html(efYtData.buttons.button_less);
        $(this).data('ef-state', 1);
      } else {
       $(this).html(efYtData.buttons.button_more);
       $(this).data('ef-state', 0);
      }
    });

    efYtInit();

    $(window).on('resize orientationchange', UIkit.Utils.debounce(function(){
        efYtItemList.slick('slickUnfilter').slick('unslick');
        if ( efYtData.search ){
        	searchFilter.val('');
        }
        efYtInit();
      }, 100)
    );

    // hide initial item in slide list
    efYtItemList.slick('slickFilter', efYtItemList.find('.slick-slide:not([data-yt-id="' + efYtData.initial_item + '"])') );

  });

  function efYtInit(){
    efYtItemList = $('.ef-yt-item-list').slick({
      arrows: false,
      dots: true,
      infinite: false,
      vertical: efYtData.layout == 'columns',
    });
  }

}(jQuery) );